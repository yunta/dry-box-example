require 'i2c/bme280'
require 'json'

$stdout.sync = true

boxen = [
	{ number: 1, color: "Grey PLA",         i2c_device: 4, i2c_address: 0x77, led_number: 1 },
	{ number: 2, color: "Galaxy Black PLA", i2c_device: 4, i2c_address: 0x76, led_number: 0 },
	{ number: 3, color: "White PLA",        i2c_device: 1, i2c_address: 0x77, led_number: 2 },
].map { |box|
	box[:sensor] = I2C::Driver::BME280.new(device: box[:i2c_device], i2c_address: box[:i2c_address])
	box[:measurements] = []
	box
}

led_driver = open("| ./pl9823","w")  #from https://gitlab.com/yunta/pl9823


log_file = open(Time.new.strftime("%Y-%m-%d %H:%M:%S")+".log","w")
log_file.sync = true
safe_humidity_range = (20.0 .. 40.0)
poll_interval = 15

start_time = Time.new
ticks = 0
leds_count = boxen.map { |box| box[:led_number] }.max
led_colors = (leds_count + 1).times.map { [0,0,0] }

loop {
	boxen.each { |box|
		measurement = { timestamp: Time.new, temperature: nil, pressure: nil, humidity: nil, errors: 0 }
		begin
			measurement[:temperature] = box[:sensor].temperature
			measurement[:pressure] = box[:sensor].pressure
			measurement[:humidity] = box[:sensor].humidity
		rescue I2C::AckError, NoMethodError, Errno::ENXIO => e
			$stderr.puts(e.inspect)
			measurement[:errors] += 1
			retry if measurement[:errors] < 10
		end
		measurement[:score] = 0.0
		if measurement[:temperature] and measurement[:humidity]
			measurement[:score] = [1.0,[0.0,1.0-(measurement[:humidity]-safe_humidity_range.begin)/(safe_humidity_range.end-safe_humidity_range.begin)].max].min
		end
		log_file.puts "%s, %d, %20s, % 8.3f, % 6.3f, % 6.3f, %d, %.3f"%[measurement[:timestamp].strftime("%Y-%m-%d %H:%M:%S"), box[:number], box[:color], measurement[:pressure], measurement[:temperature], measurement[:humidity], measurement[:errors], measurement[:score]]
		led_colors[box[:led_number]][0] = 20 * (1.0-measurement[:score])
		led_colors[box[:led_number]][1] = 20 * (measurement[:score])
	}
	led_driver.write(led_colors.map { |led| led.map { |color| "%d"%color }.join(",") }.join(" ")+"\n")

	sleep start_time + (ticks+1) * poll_interval - Time.new
	led_driver.write((leds_count + 1).times.map { "0,0,20" }.join(" ")+"\n")
	#sleep 0.1
	ticks += 1
}

